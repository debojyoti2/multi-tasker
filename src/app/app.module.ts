import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { GooglePlus } from '@ionic-native/google-plus';

const firebaseConfig = {
  apiKey: "AIzaSyBGmtj32VfqFtNaEAcRUc7kvlXKnihmqAs",
        authDomain: "fir-contact-app.firebaseapp.com",
        databaseURL: "https://fir-contact-app.firebaseio.com",
        projectId: "fir-contact-app",
        storageBucket: "fir-contact-app.appspot.com",
        messagingSenderId: "788219001157"
}

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginHomePage } from '../pages/login-home/login-home';
import { ReactiveFormsModule } from '@angular/forms';
import { Facebook } from '@ionic-native/facebook';
import { FacebookSuccessModalPage } from '../pages/facebook-success-modal/facebook-success-modal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginHomePage,
    FacebookSuccessModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig), // <-- firebase here
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    FacebookSuccessModalPage,
    LoginHomePage
  ],
  providers: [
    GooglePlus,
    StatusBar,
    Facebook,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
