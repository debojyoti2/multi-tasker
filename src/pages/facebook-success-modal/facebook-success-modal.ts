import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ListPage } from '../list/list';

@Component({
  selector: 'page-facebook-success-modal',
  templateUrl: 'facebook-success-modal.html',
})
export class FacebookSuccessModalPage {

  public user: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
  ) {
    this.user = navParams.data.fbUser;
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  public continue() {
    this.navCtrl.setRoot(ListPage);
    this.viewCtrl.dismiss();
  }


}
