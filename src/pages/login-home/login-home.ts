import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController, Platform, ModalController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ListPage } from '../list/list';
import { GooglePlus } from '@ionic-native/google-plus';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { FacebookSuccessModalPage } from '../facebook-success-modal/facebook-success-modal';

@Component({
  selector: 'page-login-home',
  templateUrl: 'login-home.html',
})
export class LoginHomePage implements OnInit {

  public normalLoginForm: FormGroup;
  public error: boolean = false;
  public googleUser: any;
  public user: Observable<firebase.User>;
  public errorMsg;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public formBuilder: FormBuilder,
    public gp: GooglePlus,
    private afAuth: AngularFireAuth,
    private platform: Platform,
    private fb: Facebook,
    public modalCtrl: ModalController
  ) {
    this.menuCtrl.close();
    this.menuCtrl.enable(false, 'myMenu');
    this.user = this.afAuth.authState;
  }

  ngOnInit() {
    this.normalLoginForm = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', [Validators.required, Validators.minLength(1)]]
    });
  }

  public normalLogin(): void {
    if (!this.normalLoginForm.invalid) {
      if (this.normalLoginForm.value.email === 'john@gmail.com' && this.normalLoginForm.value.password === '123') {
        localStorage.setItem(
          'authData', JSON.stringify({
            'name': 'John Doe',
          })
        );
        this.navCtrl.setRoot(ListPage);
      } else {
        this.error = true;
      }
    } else {
      return;
    }
  }

  public googleLogin(): void {
    if (this.platform.is('cordova')) {
      console.log("Cordova");
      this.nativeGoogleLogin();
    } else {
      this.webGoogleLogin();
    }
  }

  public googleLogout(): void {
    this.gp.logout().then();
  }

  private nativeGoogleLogin() {
    this.gp.login({}).then(res => {
      this.googleUser = res;
      this.errorMsg = res;
      console.log(res);
      this.navCtrl.setRoot(ListPage);
    }).catch(err => {
      this.errorMsg = err;
    });
  }

  async webGoogleLogin(): Promise<void> {
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      const credential = await this.afAuth.auth.signInWithPopup(provider).then(res => {
        console.log(res);
        this.navCtrl.setRoot(ListPage);
      }).catch(error => console.log(error));

    } catch (err) {
      console.log(err)
    }

  }


  public fbLogin() {
    this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
      this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        let fbUser = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'], auth: response };
        let fbSuccessModal = this.modalCtrl.create(
          FacebookSuccessModalPage, {
            'fbUser': fbUser
          });
        fbSuccessModal.present();
      });
    });
  }

  get f() { return this.normalLoginForm.controls; }
}
//  Google plus
//  From Firebase Console:
//  Web Client ID: 788219001157-calffv5vv3lnnm15ep79v1d9r08rnjue.apps.googleusercontent.com
//  Client secret: m56nlUvqmSR3mL28QSrd8mR-

